# Use an official Node.js runtime as a parent image
FROM node:14-alpine

# Set the working directory to /app
WORKDIR /app

# Copy the package.json and package-lock.json files to the working directory
# COPY my-app/package*.json ./
COPY package*.json ./   

# Install dependencies
RUN npm install

# Copy the rest of the application code to the working directory
# COPY my-app/package*.json ./
COPY . .  

# Set environment variable for React production build
ENV NODE_ENV production

# Build the React app
RUN npm run build

EXPOSE 3000

CMD [ "npm", "start" ]
